var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 3000); // Change image every 2 seconds
}

function showtext() {
   document.getElementById("fries_text").innerHTML = "<table><tr><th>Burger</th></tr><tr><td>Chicken(Grilled or Fried)</td><td></td><td>Rs 250.00</td></tr><tr><td>Black Bean Vegggie</td><td></td><td>Rs 180.00</td></tr><tr><td>Cheesey Blast</td><td></td><td>Rs 100.00</td></tr><tr><th>Fries</th></tr><tr><td>Chicken nuggets</td><td></td><td>Rs 120.00</td></tr><tr><td>Freedom Fries</td><td></td><td>Rs100.00</td></tr><tr><td>French Fries</td><td></td><td>Rs 90.00</td></tr></table>";
   document.getElementById("fries_text").style.color = "floralwhite";
   document.getElementById("fries_text").style.cssFloat = "right";
   document.getElementById("fries_text").style.fontSize = "30px";
   document.getElementById("fries_text").style.padding = "40px";
   document.getElementById("fries_text").style.fontFamily= "Helvetica Neue";
   document.getElementById("fries_text").style.fontStyle = "bold";
   
}
function showtext1() {
  document.getElementById("beer_text").innerHTML = "<table><tr><th>Mojito</th></tr><tr><td>Mint</td><td></td><td>Rs 100.00</td></tr><tr><td>Blue Berry</td><td></td><td>Rs 120.00</td></tr><tr><td>Mint & Lemonade</td><td></td><td>116.00</td></tr><tr><th>Shakes</th></tr><tr><td>Butterscotch </td><td></td><td>150.00</td></tr><tr><td>Vanilla & Choco</td><td></td><td>140.00</td></tr><tr><td>Fruit & Walnut</td><td></td><td>RS 180.00</td></tr></table>";
   document.getElementById("beer_text").style.color = "floralwhite";
   document.getElementById("beer_text").style.cssFloat = "left";
   document.getElementById("beer_text").style.fontSize = "30px";
   document.getElementById("beer_text").style.padding = "40px";
   document.getElementById("beer_text").style.fontFamily= "Helvetica Neue";
   document.getElementById("beer_text").style.fontStyle = "bold";
 
   
}
function showtext3() {
  document.getElementById("cupcake_text").innerHTML = "<table><tr><th>Cakes</th></tr><tr><td>Red Velvet</td><td></td><td>Rs 450.00</td></tr><tr><td>Strawberry</td><td></td><td>Rs 350.00</td></tr><tr><td>Chocolaty Sizzle</td><td></td><td>Rs 600.00</td></tr><tr><td>Fruit & Walnut</td><td></td><td>Rs 700.00</td></tr><tr><th>Cup Cakes</th></tr><tr><td>Cookies & Cream</td><td></td><td>Rs 100.00</td></tr><tr><td>Salted & Caramel</td><td></td><td>Rs 90.00</td></tr><tr><td>Raspberry Chocolate</td><td></td><td>Rs108.00</td></tr></table>";
   document.getElementById("cupcake_text").style.color = "white";
   document.getElementById("cupcake_text").style.cssFloat = "left";
   document.getElementById("cupcake_text").style.fontSize = "30px";
   document.getElementById("cupcake_text").style.padding = "40px";
   document.getElementById("cupcake_text").style.fontFamily= "Helvetica Neue";
   document.getElementById("cupcake_text").style.fontStyle = "bold";
 
   
}
function showtext2() {
  document.getElementById("pizza_text").innerHTML = "<table><tr><th>Pizza</th></tr><tr><td>Margarita</td><td></td><td>Rs 250.00</td></tr><tr><td>Peperoni</td><td></td><td>Rs 270.00</td></tr><tr><td>Salami Cheesy</td><td></td><td>Rs 300.00</td></tr><tr><td>Mexican Sizzle</td><td></td><td>Rs 350.00</td></tr><tr><th>Puffs</th></tr><tr><td>Mexican Veggie</td><td></td><td>Rs 110.00</td></tr><tr><td>Garlic Cheesy</td><td></td><td>Rs 120.00</td></tr><tr><td>Chicken Box</td><td></td><td>Rs 170.00</td></tr></table>";
   document.getElementById("pizza_text").style.color = "white";
   document.getElementById("pizza_text").style.cssFloat = "right";
   document.getElementById("pizza_text").style.fontSize = "30px";
   document.getElementById("pizza_text").style.padding = "40px";
   document.getElementById("pizza_text").style.fontFamily= "Helvetica Neue";
   document.getElementById("pizza_text").style.fontStyle = "bold";
 
   
}
function showtext_after() {
  document.getElementById("fries_text").innerHTML = "";
   document.getElementById("fries_text").style.color = "red";
   document.getElementById("beer_text").innerHTML = "";
   document.getElementById("beer_text").style.color = "red";
   document.getElementById("pizza_text").innerHTML = "";
   document.getElementById("cupcake_text").innerHTML = "";
}

